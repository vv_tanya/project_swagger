package com.exampleswagger.demo.repository;

import com.exampleswagger.demo.model.PingModel;
import org.springframework.stereotype.Repository;


import java.util.*;

@Repository
public class PingRepository {

    private List<PingModel> storage = new ArrayList<>();


    public void add(PingModel model) {
        storage.add(model);
    }

    public List<PingModel> getList() {
        return storage;
    }
}
